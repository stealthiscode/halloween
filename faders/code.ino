// NOTE: other end of LEDs are tied through 330 ohm resistor to +

int led1 = 3;           // the pin that the LED is attached to
int brightness1 = 40;  // how bright the LED is
int fadeAmount1 = 10;    // how many points to fade the LED by

int led2 = 5;          // the pin that the LED is attached to
int brightness2 = 80;   // how bright the LED is
int fadeAmount2 = 10;    // how many points to fade the LED by

int led3 = 6;          // the pin that the LED is attached to
int brightness3 = 120;   // how bright the LED is
int fadeAmount3 = 10;    // how many points to fade the LED by

int led4 = 9;          // the pin that the LED is attached to
int brightness4 = 160;   // how bright the LED is
int fadeAmount4 = 10;    // how many points to fade the LED by

int led5 = 10;          // the pin that the LED is attached to
int brightness5 = 200;   // how bright the LED is
int fadeAmount5 = 10;    // how many points to fade the LED by

int led6 = 11;          // the pin that the LED is attached to
int brightness6 = 240;   // how bright the LED is
int fadeAmount6 = 10;    // how many points to fade the LED by

// the setup routine runs once when you press reset:
void setup()  { 
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);  
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(led5, OUTPUT);
  pinMode(led6, OUTPUT);
} 

// the loop routine runs over and over again forever:
void loop()  { 
  analogWrite(led1, brightness1);    
  analogWrite(led2, brightness2);      
  analogWrite(led3, brightness3);      
  analogWrite(led4, brightness4);      
  analogWrite(led5, brightness5);      
  analogWrite(led6, brightness6);      

  fade(brightness1, fadeAmount1);
  fade(brightness2, fadeAmount2);  
  fade(brightness3, fadeAmount3);  
  fade(brightness4, fadeAmount4);  
  fade(brightness5, fadeAmount5);  
  fade(brightness6, fadeAmount6);  
}

void fade(int& brightness, int& fadeAmount) {
  // reverse the direction of the fading at the ends of the fade: 
  if (brightness <= (random(20,40) * 2) || brightness >= (random(11,120) * 2)) {
    fadeAmount = -fadeAmount ; 
  }     

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // change rate of brightness change depending on how bright
  // which allows smoother fades across all brightnesses
  if (brightness >= 200 && brightness <= 255) {
    delay(50);                            
  }
  else {
    delay(30);
  }

}
