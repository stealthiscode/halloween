int intSize = sizeof(int);
int pirSensorPin = 6;
int gargoyleRelays[] = {2, 3};
int foggerRelays[] = {4};
int foggerReadyPin = A0;

void initialize_input(int pin, const char* type) {
  Serial.print("Initializing INPUT pin: ");
  Serial.println(pin);
  pinMode(pin, INPUT);
  if (type == "digital") {
    digitalWrite(pin, LOW);
  }
}

void calibrate() {
  // i wonder what this really does...
  Serial.print("calibrating sensor ");
    for(int i = 0; i < 30; i++) {
      Serial.print(".");
      delay(1000);
    }
  Serial.println(" done");
  Serial.println("SENSOR ACTIVE");
  delay(50);
}

void init_all() {
  initialize_outputs(gargoyleRelays, sizeof(gargoyleRelays));
  initialize_outputs(foggerRelays, sizeof(foggerRelays));
  initialize_input(pirSensorPin, "digital");
  initialize_input(foggerReadyPin, "analog");
  calibrate();
}

void initialize_outputs(const int* pins, size_t size) {
  for (int i = 0; i < (size/intSize); i++) {
    Serial.print("Initializing OUTPUT pin: ");
    Serial.println(pins[i]);
    pinMode(pins[i], OUTPUT);
    digitalWrite(pins[i], LOW);
  }
}

boolean check_analog_input(int pin) {
  int value = analogRead(pin);
  float voltage = value * (5.0 / 1023.0);
  Serial.println(voltage);
  return (voltage > 1);
  /* if (value > 1) {
    return true;
  } else {
    return false;
  } */
}

boolean check_digital_input(int pin) {
  int value = digitalRead(pin);
  Serial.println(value);
  return (value != 1);
  /* if (value == 0) {
    return false;
  } else {
    return true;
  } */
}

void enable_outputs(const int* pins, size_t size) {
  for (int i = 0; i < (size/intSize); i++) {
    Serial.print("set pin HIGH  ");
    Serial.println(pins[i]);
    //digitalWrite(pins[i], HIGH);
  }
}

void setup() {
  Serial.begin(9600);
  init_all();
}

void loop() {
  delay(1000);
  
  // loop until fog machine is in ready state
  boolean ready = check_analog_input(foggerReadyPin);
  if (ready == false) {
    Serial.println("fog machine not ready");
    return;
  }
  Serial.println("fog machine ready");

  // start checking for motion
  boolean motion = check_digital_input(pirSensorPin);
  while (motion == false) {
    Serial.println("no motion");
    delay(500);
    boolean motion = check_digital_input(pirSensorPin);
  }
  Serial.println("motion detected");

  // start fog, wait 5s, start gargoyles
  enable_outputs(foggerRelays, sizeof(foggerRelays));
  delay(5000);
  enable_outputs(gargoyleRelays, sizeof(gargoyleRelays));

  // wait another 20s and stop everything
  delay(20000);
  init_all();
}
  

